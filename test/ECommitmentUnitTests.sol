pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ECommitment.sol";

contract ECommitmentUnitTests {

    address[] addresses;
    address testContractAddress;

    constructor() public {
        testContractAddress = msg.sender;
    }

    function testAgreementIsSuccesfullyCreated() public {
        ECommitment dContract = ECommitment(DeployedAddresses.ECommitment());
        uint weis = 1500000000000000000 wei; // 1.5 ether
        addresses.push(0xE0f5206BBD039e7b0592d8918820024e2a7437b9);
        addresses.push(0xF4b9ed39dD183504252Ee5995C58DAc8197fa12D);
        addresses.push(0x82A5596b4ec22b0DE9Af79F9958127119cC7FA77);
        uint expectedPenaltyPercentage = 50;
        string memory expectedDescription = "I commit to write unit tests";
        ECommitment.CommitmentState expectedState = ECommitment.CommitmentState.WAITING_FOR_PARTICIPANTS;

        // create the contract
        dContract.makeCommitment(addresses, weis, expectedPenaltyPercentage, expectedDescription);
        uint id;
        uint noOfParticipants;
        uint stakeWei;
        uint penaltyPercentage;
        string memory description;
        ECommitment.CommitmentState state;
        (id,noOfParticipants,stakeWei,penaltyPercentage,description,state) = dContract.commitments(0);

        // assert that new agreement is created properly
        Assert.equal(0, id, "id should be 0 (first contract)");
        Assert.equal(3, noOfParticipants, "there should exist 3 participants");
        Assert.equal(weis, stakeWei, "wrong amount of stake (wei)");
        Assert.equal(expectedPenaltyPercentage, penaltyPercentage, "wrong penalty %");
        Assert.equal(description, expectedDescription, "wrong description");
        Assert.isTrue(state == expectedState, "the commitment should be waiting for participants");

        // ... and also assert the participants
        assertParticipant(dContract, 0, 0, addresses[0], false, false);
        assertParticipant(dContract, 0, 1, addresses[1], false, false);
        assertParticipant(dContract, 0, 2, addresses[2], false, false);
    }

        /*
        * helper function, to easily assert the state of participants in commitments
        */
    function assertParticipant(ECommitment deployedContract, uint contractId, uint participantId, address expectedAddress,
        bool expectedIsCommited, bool expectedIsSatisfied) private {
        address addr;
        bool isCommited;
        bool isSatisfied;
        (addr, isCommited, isSatisfied) = deployedContract.participants(contractId, participantId);
        Assert.equal(addr, expectedAddress, "wrong participant address");
        Assert.equal(isCommited, expectedIsCommited, "wrong commitment state of participant");
        Assert.equal(isSatisfied, expectedIsSatisfied, "wrong satisfaction state of participant");
    }
}
