/*
*  These tests are one level of abstraction higher than the unit tests,
*  making them roughly equivalent to Spring Applications' integration tests.
*
*  The idea is to test the deployed contract via javascript, simulating how
*  a user (or rather a consumer, like javascript client code) would use our contract
*/

/*

var SimpleStorage = artifacts.require("./SimpleStorage.sol");

contract('SimpleStorage', function(accounts) {

  it("...should store the value 89.", function() {
    return SimpleStorage.deployed().then(function(instance) {
      simpleStorageInstance = instance;

      return simpleStorageInstance.set(89, {from: accounts[0]});
    }).then(function() {
      return simpleStorageInstance.get.call();
    }).then(function(storedData) {
      assert.equal(storedData, 89, "The value 89 was not stored.");
    });
  });

});
*/
