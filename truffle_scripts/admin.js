module.exports = function(callback) {
	const artifacts = require('../build/contracts/ECommitment.json')
	const contract = require('truffle-contract')
	const ECommitment = contract(artifacts);

	let commitmentId = 0;
	let deployedInstance;
	let commitmentPropertyMap = {};

	ECommitment.setProvider(web3.currentProvider);
	ECommitment.deployed().then(function(instance) {
		deployedInstance = instance;
		let commitment = deployedInstance.commitments(commitmentId);
		return commitment;
	}).then(function(result) {
		//let flushResult = deployedInstance.flush({from: web3.eth.accounts[0], gas: 100000});
		//let flushResult = deployedInstance.owner();
		let flushResult = deployedInstance.eject({from: web3.eth.accounts[0], gas: 100000});
		return flushResult;
	}).then(function(result) {
		if (!result.logs) {
			console.log(result)
			return;
		}
		for (var i = 0; i < result.logs.length; i++) {
			var log = result.logs[i];
			if(log.event == "DebugMessage") {
				console.log("Debug message: " + log.args.message);
			}
		}
	}).catch(function(error) {
	  console.error('ERROR: ' + error)
	  console.log(error)
	}).finally(function() {
		console.log("Done!");
		callback();
	});

	function getMethods(obj) {
	    var res = [];
	    for(var m in obj) {
	        if(typeof obj[m] == "function") {
	            res.push(m)
	        }
	    }
    	return res;
	}

}
