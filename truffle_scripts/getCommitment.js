//require("C:\\Users\\Takis\\workspace\\ecommitment\\truffle_scripts\\script.js")
module.exports = function(callback) {
	const artifacts = require('../build/contracts/ECommitment.json')
	const contract = require('truffle-contract')
	const ECommitment = contract(artifacts);

	let commitmentId = 0;
	let deployedInstance;
	let commitmentPropertyMap = {};

	ECommitment.setProvider(web3.currentProvider);
	ECommitment.deployed().then(async function(instance) {
		deployedInstance = instance;
		//console.log(getMethods(deployedInstance))
		console.log("Balance: " + await deployedInstance.checkBalance());
		let commitment = deployedInstance.commitments(commitmentId);
		return commitment;
	}).then(async function(result) {
		commitmentPropertyMap.id = result[0].toNumber();
		commitmentPropertyMap.noOfParticipants = result[1].toNumber();
		commitmentPropertyMap.deposit = result[2].toNumber();
		commitmentPropertyMap.penalty = result[3].toNumber();
		commitmentPropertyMap.description = result[4];
		let stateString;
		switch(result[5].toNumber()) {
			case 0:
				stateString = "WAITING_FOR_PARTICIPANTS";
				break;
			case 1:
				stateString = "STARTED";
				break;
			case 2:
				stateString = "ENDED";
				break;
		}
		commitmentPropertyMap.state = stateString;
		commitmentPropertyMap.participants = [];
		for(var i = 0; i < commitmentPropertyMap.noOfParticipants; i++) {
			let participant = await deployedInstance.participants(commitmentId, i);
			let prettyObject = {}
			prettyObject.address = participant[0]
			prettyObject.deposited = participant[1]
			let satisfactionString;
			switch(participant[2].toNumber()) {
				case 0:
					satisfactionString = "UNDECIDED";
					break;
				case 1:
					satisfactionString = "SATISFIED";
					break;
				case 2:
					satisfactionString = "UNSATISFIED";
					break;
			}
			prettyObject.satisfaction = satisfactionString;
			commitmentPropertyMap.participants.push(prettyObject);
		}
	}).catch(function(error) {
	  console.error('ERROR: ' + error)
	}).finally(function() {
		console.log("COMMITMENT #" + commitmentId + ": \n");
		console.log(commitmentPropertyMap);
		callback();
	});


		function getMethods(obj) {
		    var res = [];
		    for(var m in obj) {
		        if(typeof obj[m] == "function") {
		            res.push(m)
		        }
		    }
	    	return res;
		}
}
