module.exports = function(callback) {
	const artifacts = require('../build/contracts/ECommitment.json')
	const contract = require('truffle-contract')
	const ECommitment = contract(artifacts);

	let commitmentId = 0;
	let deployedInstance;
	let commitmentPropertyMap = {};

	ECommitment.setProvider(web3.currentProvider);
	ECommitment.deployed().then(function(instance) {
		deployedInstance = instance;
		let commitment = deployedInstance.commitments(commitmentId);
		return commitment;
	}).then(function(result) {
		//set the satisfaction
		let satisfactionValue = 2;
		console.log("Setting satisfaction to " + satisfactionValue);
		let satisfactionResult = deployedInstance.setSatisfaction(commitmentId, satisfactionValue,
			{from: web3.eth.accounts[0], gas: 100000});
		//let satisfactionResult = deployedInstance.test(1,2,{from: web3.eth.accounts[2], gas: 100000});
		return satisfactionResult;
	}).then(function(result) {
		for (var i = 0; i < result.logs.length; i++) {
			var log = result.logs[i];
			if(log.event == "DebugMessage") {
				console.log("Debug message: " + log.args.message);
			}
		}
	}).catch(function(error) {
	  console.error('ERROR: ' + error)
	  console.log(error)
	}).finally(function() {
		console.log("Done!");
		callback();
	});
}
