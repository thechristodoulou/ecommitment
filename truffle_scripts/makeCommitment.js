//require("C:\\Users\\Takis\\workspace\\ecommitment\\truffle_scripts\\script.js")
module.exports = function(callback) {
	const artifacts = require('../build/contracts/ECommitment.json')
	const contract = require('truffle-contract')
	const ECommitment = contract(artifacts);

	let deployedInstance;
	let newEventId;
	let newCommitmentMessage;

	ECommitment.setProvider(web3.currentProvider);
	ECommitment.deployed().then(function(instance) {
		deployedInstance = instance;
		let result = deployedInstance.makeCommitment([
			//'545',
			'0x3a5d0a6d4d64e7243ff452698a63627d127e4d40'
			//'123'
		], 1000000000, 50, 'we agree to love the paw', {from: web3.eth.accounts[0], gas: 350000});
		return result;
	}).then(function(result) {
		for (var i = 0; i < result.logs.length; i++) {
				var log = result.logs[i];
				if (log.event == "NewCommitment") {
					newEventId = log.args.id.toNumber();
					console.log("Commitment created!");
				} else if(log.event == "DebugMessage") {
					console.log("Debug message: " + log.args.message);
				}
		}
	}).then(function(result) {
		let commitment = deployedInstance.commitments(newEventId);
		return commitment;
	}).then(function(result) {
		console.log("Success! Created new commitment with id: " + result[0].toNumber());
	}).catch(function(error) {
	  console.error('ERROR: ' + error)
	}).finally(function() {
		callback();
	});
}
