pragma solidity ^0.4.2;

contract ECommitment {

    event NewCommitment(uint id);
	event DebugMessage(string message);

	enum CommitmentState {WAITING_FOR_PARTICIPANTS, IN_EFFECT, ENDED}
	enum SatisfactionState {UNDECIDED, SATISFIED, UNSATISFIED}

	struct Participant {
		address addr;
		bool hasDeposited;
		SatisfactionState satisfaction;
	}

	struct Commitment {
		uint id;
		uint noOfParticipants;
        uint depositWei;
        uint penaltyPercentage;
		string description;
		CommitmentState state;
	}

	mapping (uint => Commitment) public commitments;
	mapping (uint => Participant[]) public participants;
	uint private nextCommitmentId;

    // ================ ADMIN STUFF ===================================
    address public owner;

    modifier onlyOwner() {
        assert(msg.sender == owner);
        _;
    }

	constructor() public {
		nextCommitmentId = 0;
        owner = msg.sender;
	}

    function checkBalance() view external returns (uint) {
        return address(this).balance;
    }

    function flush() external onlyOwner {
        owner.transfer(address(this).balance);
    }

    function eject() external onlyOwner {
        selfdestruct(owner);
    }

    function () public payable { // default receptor of ether

    }
    // ==================== END OF ADMIN STUFF ===========================

	function makeCommitment(address[] participantsAddresses, uint depositWei, uint penaltyPercentage, string description) external {
	    if (participantsAddresses.length > 10 ) { // to avoid infinite gas, limit the loop to max 10 participants
	        emit DebugMessage("Cannot have more than 10 participants in a commitment");
	        return;
	    }

		// create the commitment
		Commitment memory newCommitment;
		newCommitment.id = nextCommitmentId;
		newCommitment.noOfParticipants = participantsAddresses.length;
        newCommitment.depositWei = depositWei;
        newCommitment.penaltyPercentage = penaltyPercentage;
		newCommitment.description = description;
		newCommitment.state = CommitmentState.WAITING_FOR_PARTICIPANTS;

		// this will write it to storage, since the mapping 'commitments' is in storage
		commitments[nextCommitmentId] = newCommitment;

		// once its in storage, add the participants
		for (uint i = 0; i < participantsAddresses.length; i++) {
			participants[nextCommitmentId].push(Participant(participantsAddresses[i], false, SatisfactionState.UNDECIDED));
		}

        // done! emit and increment
		emit NewCommitment(nextCommitmentId);
		nextCommitmentId++;
	}

	function commit(uint commitmentId) payable external {
        //address(this).balance += msg.value;

        // find and assert that the sender in the commitment's participants
        uint matchedParticipantId = 0;
        for (uint i = 0; i < commitments[commitmentId].noOfParticipants; i++) {
			if (participants[commitmentId][i].addr == msg.sender) {
                matchedParticipantId = (i + 1);
                break;
            }
		}

		if (matchedParticipantId == 0) {
		    emit DebugMessage("Wrong sender, refunding");
		    require(msg.sender.send(msg.value));
		    return;
		}

        // was the expected amount received?
        uint depositDifference = uint(msg.value - commitments[commitmentId].depositWei);
        if (depositDifference == 0) {
            emit DebugMessage("All good! Participant should now be commited.");
            participants[commitmentId][matchedParticipantId-1].hasDeposited = true;
        } else if (depositDifference > 0) { // too much was sent
            emit DebugMessage("Too much was sent.");
            participants[commitmentId][matchedParticipantId-1].hasDeposited = true;
            msg.sender.transfer(depositDifference);
        } else if (depositDifference < 0) {
            emit DebugMessage("Too little was sent, will refund.");
            msg.sender.transfer(msg.value);
        }

        //  also check/set commitment global state!!!
        uint commitedParticipants = 0;
        for (i = 0; i < commitments[commitmentId].noOfParticipants; i++) {
			if (participants[commitmentId][i].hasDeposited) {
                commitedParticipants = commitedParticipants + 1;
            }
		}
        if (commitedParticipants == commitments[commitmentId].noOfParticipants) {
            commitments[commitmentId].state = CommitmentState.IN_EFFECT;
            emit DebugMessage("Commitment is now in effect!");
        }
	}

	function setSatisfaction(uint commitmentId, uint satisfactionState) external {
        // allowed values are 1:satisfied or 2:unsatisfied
        require(satisfactionState == 1 || satisfactionState == 2);

        // find and assert that the sender in the commitment's participants AND that he or she has already deposited
        uint matchedParticipantId = 0;
        for (uint i = 0; i < commitments[commitmentId].noOfParticipants; i++) {
			if (participants[commitmentId][i].addr == msg.sender) {
                if (participants[commitmentId][i].hasDeposited) {
                    matchedParticipantId = (i + 1);
                    break;
                }
            }
		}

        if (matchedParticipantId == 0) {
		    emit DebugMessage("Sender was either not in the commitment or had not deposited.");
            require(false);
		}

        // finally, set the participants satisfaction state
        participants[commitmentId][matchedParticipantId-1].satisfaction = SatisfactionState(satisfactionState);

        // final update of contract state - most important part
        uint satisfiedParticipants = 0;
        uint unsatisfiedParticipants = 0;
        for (i = 0; i < commitments[commitmentId].noOfParticipants; i++) {
			if (participants[commitmentId][i].satisfaction == SatisfactionState.UNSATISFIED) {
                unsatisfiedParticipants = unsatisfiedParticipants + 1;
            } else if (participants[commitmentId][i].satisfaction == SatisfactionState.SATISFIED) {
                satisfiedParticipants = satisfiedParticipants + 1;
            }
		}
        if (satisfiedParticipants == commitments[commitmentId].noOfParticipants) {
            // everyone is happy
            emit DebugMessage("Everyone is happy, will do (almost) full refunds.");
            commitments[commitmentId].state = CommitmentState.ENDED;
        }
        if (unsatisfiedParticipants > 0) { // if at least one is unhappy, kill it
            emit DebugMessage("At least one dude is unhappy, will do penalized refunds.");
            commitments[commitmentId].state = CommitmentState.ENDED;
        }
    }

}
