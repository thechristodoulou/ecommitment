// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    ganache: {
      //host: '192.168.0.105',
      host: 'localhost',
      //from: "0x3a5d0a6d4d64e7243ff452698a63627d127e4d40", //or any address you want
      port: 7545,
      network_id: '*' // Match any network id
    },
    rinkeby: { // (local) geth node that should be connected to rinkeby
        host: '192.168.0.108',
        port: 7545,
        from: "0xd2b1e7dbf32533012e4e3a4e781d842861bc2c57",
        network_id: '4'
      }
    }
}
