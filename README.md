## To start the node server:
`node run start`

## To deploy a contract
`truffle compile`

`truffle connect <your blockchain's rpc URL)`

`truffle migrate`