import React  from 'react';
import Header from '../header'
import { Container, Row, Col } from 'reactstrap';

class FourOhFour extends React.Component {

    render() {
        return (
            <Container>
            <Row>
                <Header/>
            </Row>
            <Row>
                <Col>
                    <h1 className="pt-3">404</h1>
                    <h4>Sometimes you do not find what you seek.</h4>
                </Col>
            </Row>
            </Container>
        )
    }
}

export default FourOhFour
