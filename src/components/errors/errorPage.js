import React  from 'react';
import Header from '../header'
import { connect } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';

class AccountError extends React.Component {

    render() {
        return (
            <Container>
            <Row>
                <Header/>
            </Row>
            { this.props.error && this.props.error.noWeb3js &&
                <Row>
                    <Col>
                        <h1 className="pt-3">Error</h1>
                        <h4>You need <a target="_blank" rel="noopener noreferrer" href="https://metamask.io/">MetaMask</a> to use this website.</h4>
                        <span>If you are interested in Ethereum dApps, give it a try.</span>
                    </Col>
                </Row>
            }
            { this.props.error && this.props.error.type === "no-connection" &&
                <Row>
                    <Col>
                        <h1 className="pt-3">Error</h1>
                        <h4>There was a problem connecting to the blockchain</h4>
                        <span>If you do not know why this occured, <a href="mailto: ecommitmentbugs?@gmail.com">let me know</a> by including the message below.</span>
                        <p>Looking at the console might also help.</p>
                        <pre className="p-1 m-1 border">{this.props.error.stack}</pre>
                    </Col>
                </Row>
            }
            { this.props.error && this.props.error.type === "no-contract" &&
                <Row>
                    <Col>
                        <h1 className="pt-3">where is contract?!</h1>
                    </Col>
                </Row>
            }
            { this.props.error && (this.props.error.type === "unexpected" || !this.props.error.type) &&
                <Row>
                    <Col>
                        <h1 className="pt-3">Unexpected Error</h1>
                    </Col>
                </Row>
            }
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        error: state.account.error
    };
}

function mapDispatchToProps(dispatch) {
    return {
        // nothing is needed here, yet
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountError);
