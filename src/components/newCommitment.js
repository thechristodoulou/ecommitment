import React from 'react';
import Header from './header'
import { Container, Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { createNewCommitment, removeCommitment } from '../actions/commitments';
import { isNumeric } from '../utils/miscUtils';
import { getEtherscanHtmlLinkForAddress } from '../utils/miscUtils';
import $ from 'jquery';

class NewCommitment extends React.Component {

    constructor(props) {
        super(props);
        this.createNewCommitment = this.createNewCommitment.bind(this);
    }

    render() {
        return (
            <Container>
                <Row>
                    <Header/>
                </Row>

                <Row className="pt-3">
                    <Col xs="3" className="pt-1">
                        <p className="text-right">Deposit per participant:</p>
                    </Col>
                    <Col xs="4">
                        <input id="depositEther" className="form-control" type="text" placeholder="Amount in Ether..." />
                    </Col>
                </Row>

                <Row className="pt-3">
                    <Col xs="3" className="pt-1">
                        <p className="text-right">Penalty:</p>
                    </Col>
                    <Col xs="4">
                        <input id="penalty" className="form-control" type="text" placeholder="Percentage as a number e.g. 50" />
                    </Col>
                    <Col className="pt-1 pl-0">
                        <p className="">% of the deposit.</p>
                    </Col>
                </Row>

                <Row className="pt-3">
                    <Col xs="3" className="pt-1">
                        <p className="text-right">Description:</p>
                    </Col>
                    <Col xs="4">
                        <input id="description" className="form-control" type="text" placeholder="What is this commitment about?" />
                    </Col>
                </Row>

                <Row className="pt-3">
                    <Col xs="3" className="pt-1">
                        <p className="text-right">Participants:</p>
                    </Col>
                    <Col xs="4">
                        <input id="participant1" className="form-control mb-2" type="text" placeholder="Participant's #1 Ethereum address" />
                        <input id="participant2" className="form-control mb-2" type="text" placeholder="Participant's #2 Ethereum address" />
                        <input id="participant3" className="form-control" type="text" placeholder="Participant's #3 Ethereum address" />
                    </Col>
                </Row>

                <Row className="pt-3">
                    <Col xs="1" className="offset-sm-2">
                        <button type="button" className="btn btn-light" onClick={this.createNewCommitment}>Create</button>
                    </Col>
                    <Col xs="7">
                        <p className="mt-1 ml-1 text-left" id="submitTxMessage" dangerouslySetInnerHTML={{ __html: this.createTxSubmittedMessage() }}/>
                    </Col>
                </Row>

                <Row>
                    <Col xs="9" className="offset-sm-3">
                        <span id="newTxMessagee" dangerouslySetInnerHTML={{ __html: this.createNewTxMessage() }}/>
                    </Col>
                </Row>
            </Container>
        )
    }

    componentWillUnmount() {
        this.props.clearCommitment();
    }

    createNewCommitment() {
        let web3jsInstance = this.props.account.provider.web3;
        let result = this.validateAndGetAllFields(web3jsInstance);

        if (result.errors && result.errors.length > 0) {
            let firstErrorText = result.errors[0];
            $("#submitTxMessage").html("<span class='text-danger'>" + firstErrorText + "</span>");
        } else {
            this.props.createNewCommitment(web3jsInstance, this.props.account.network.id, result.participants, result.depositWei,
                result.penaltyPercentage, result.description);
        }
    }

    /*
    * Goes through all the necessary fields and returns an object with either their validated values or any validation
    * errors that occured
    */
    validateAndGetAllFields(web3) {
        let potentialCommitment = {
            errors: []
        };

        let depositEtherRaw = document.getElementById('depositEther').value;
        if (!isNumeric(depositEtherRaw)) {
            potentialCommitment.errors.push("Deposit is not a number.")
        } else {
            let wei = this.props.account.provider.web3.toWei(depositEtherRaw, 'ether');
            potentialCommitment.depositWei = wei;
        }

        let penaltyRaw = document.getElementById('penalty').value;
        if (!isNumeric(penaltyRaw)) {
            potentialCommitment.errors.push("Penalty is not a number.")
        } else {
            potentialCommitment.penaltyPercentage = penaltyRaw.trim();
        }

        potentialCommitment.description = document.getElementById('description').value;

        let participantsArray = [
            document.getElementById('participant1').value,
            document.getElementById('participant2').value,
            document.getElementById('participant3').value
        ];

        let actualParticipants = participantsArray.filter(participant => {
            if(participant && !web3.isAddress(participant.trim())) {
                potentialCommitment.errors.push("Invalid address: " + participant);
                return null;
            }
            return (participant !== undefined && web3.isAddress(participant.trim())) ? participant.trim() : undefined;
        });

        if (actualParticipants.length > 1) {
            potentialCommitment.participants = actualParticipants;
        } else {
            potentialCommitment.errors.push("At least 2 participants are needed.")
        }

        return potentialCommitment;
    }

    createTxSubmittedMessage() {
         if (this.props.commitments.isQuerying) {
            return "<span>Submitting...</span>";
        } else if (this.props.commitments.error) {
            return "<span class='text-danger'>" + this.props.commitments.error.message + "</span>";
        } else if (this.props.commitments.buffer) {
            return "<span>Transaction submitted.</span>";
        }
    }

    createNewTxMessage() {
        if (this.props.commitments.buffer) {
            let newCommitment = this.props.commitments.buffer;
            let link = getEtherscanHtmlLinkForAddress(this.props.account.network.id, newCommitment.tx);
            return "<spam>See it here: " + link + "</span><br/><span>Your new commitment ID: <strong>" + newCommitment.id + "</strong><br/>Please note these down.";

        } else if(this.props.commitments.error) {
            // is there need to display something here. think about it TODO
        }
    }
}

function mapStateToProps(state) {
    return {
        account: state.account,
        commitments: state.commitments
    };
}

function mapDispatchToProps(dispatch) {
    return {
        createNewCommitment: function(web3, networkId, participantsArray, depositEther, penaltyPercentage, description) {
            dispatch(createNewCommitment(web3, networkId, participantsArray, depositEther, penaltyPercentage, description));
        },
        clearCommitment: function() {
            dispatch(removeCommitment());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCommitment);
