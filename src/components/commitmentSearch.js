import React from 'react';
import Header from './header'
import SingleCommitment from './singleCommitment'
import { Container, Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { fetchCommitmentAndParticipants, removeCommitment } from '../actions/commitments';
import { isNumeric } from '../utils/miscUtils';
import $ from 'jquery';

class CommitmentSearch extends React.Component {

    constructor(props) {
        super(props);
        this.searchForCommitment = this.searchForCommitment.bind(this);
    }

    render() {
        return (
            <Container>
            <Row>
                <Header/>
            </Row>
            <Row className="flex-vertical-align pt-3">
                <Col xs="3">
                    <div className="active-black-4">
                        <input id="commitment-search" className="form-control" type="text" placeholder="Commitment ID..." />
                    </div>
                </Col>
                <Col xs="1.5">
                    <button id="search-button" type="button" className="btn btn-light" onClick={this.searchForCommitment}>Search</button>
                </Col>
                <Col xs="6">
                    <span id="message" dangerouslySetInnerHTML={{ __html: this.createStatusMessage() }}/>
                </Col>
            </Row>
            { this.props.commitments.buffer && this.props.commitments.buffer.participants &&
                <SingleCommitment commitment={this.props.commitments.buffer}/>
            }
            </Container>
        )
    }

    componentWillMount() {
        this.attachEventsToSearchField();
    }

    attachEventsToSearchField() {
        $('body').on('keyup', '#commitment-search', function(a) {
            if (a.keyCode === 13) { // enter
                $("#search-button").trigger("click");
            }
        });
    }

    searchForCommitment() {
        let id = $('#commitment-search').val();
        if(!id.trim() || id.trim() === "") {
            return;
        } else if(id.trim() !== "" && isNumeric(id)) {
            this.props.searchForCommitment(this.props.account.provider.web3, this.props.account.network.id, id);
        } else {
            $("#message").html("<span class='text-danger'>Non-numeric value.</span>");
        }
    }

    createStatusMessage() {
        if (this.props.commitments.isQuerying) {
            return "<span>Querying...</span>";
        } else if (this.props.commitments.error) {
            return "<span class='text-danger'>" + this.props.commitments.error.message + "</span>";
        } else {
            return "<span></span>";
        }
    }

    componentWillUnmount() {
        this.props.clearCommitment();
    }
}

function mapStateToProps(state) {
    return {
        commitments: state.commitments,
        account: state.account
    };
}

function mapDispatchToProps(dispatch) {
    return {
        searchForCommitment: function(web3, networkId, contractId) {
            dispatch(fetchCommitmentAndParticipants(web3, networkId, contractId));
        },
        clearCommitment: function() {
            dispatch(removeCommitment());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CommitmentSearch);
