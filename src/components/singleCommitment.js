import React from 'react';
import { connect } from 'react-redux';
import ParticipantRow from './participantRow';
import { Col, Row } from 'reactstrap';

class SingleCommitment extends React.Component {
    render() {
        if (this.props.commitment) {
            return (
                <div>
                    <Row style={{'margin-top': '15px'}}>
                        <Col style={{'font-weight': 'bold'}} className="" xs="5">Deposit per Participant:</Col>
                        <Col xs="7"><span dangerouslySetInnerHTML={{ __html: this.getDepositHtmlString() }}/></Col>
                    </Row>
                    <Row style={{'margin-top': '15px'}}>
                        <Col style={{'font-weight': 'bold'}} className="" xs="5">Penalty:</Col>
                        <Col xs="7"><strong>{ this.getSafeProperty('penaltyPercentage') }</strong>%</Col>
                    </Row>
                    <Row style={{'margin-top': '15px'}}>
                        <Col style={{'font-weight': 'bold'}} className="" xs="5">Description:</Col>
                        <Col xs="7">
                            { this.getSafeProperty('description') &&
                                <p className="mb-1 montserrat-font">
                                { this.getSafeProperty('description') }
                                </p>
                            }
                            { !this.getSafeProperty('description') &&
                                <p className="mb-1">No description provided.</p>
                            }
                        </Col>
                    </Row>
                    <Row style={{'margin-top': '15px'}}>
                        <Col style={{'font-weight': 'bold'}} className="" xs="5">Participants:</Col>
                    </Row>
                    {
                        this.props.commitment.participants.map(participant => {
                            return (
                                <div style={{'font-weight': 'bold', 'margin': '5px'}}  key={participant.address}>
                                    <ParticipantRow participant={participant} depositWei={this.getSafeProperty('depositWei')} commitmentId={this.getSafeProperty('id')} networkId={this.props.account.network.id}/>
                                </div>
                            )
                        })
                    }
                </div>
            )
        } else {
            return null;
        }
    }

    componentWillMount() {
        //console.log("Inside singleComitment.js");
        //console.log(this.props);
    }

    getSafeProperty(property) {
        if (this.props.commitment === undefined) {
            return "";
        }
        let prop = this.props.commitment[property];
        return (prop === undefined) ? "" : prop;
    }

    getDepositHtmlString() {
        let weis = parseInt(this.getSafeProperty('depositWei'), 0);
        return "Deposit per participant: <strong>" + this.props.account.provider.web3.fromWei(weis, 'ether') + " Ether</strong> (" + weis + " weis)";
    }

    isUserMemberOfCommitment(participantAddress) {
        return participantAddress === this.props.account.address;
    }
}

function mapStateToProps(state) {
    return {
        account: state.account,
    };
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleCommitment);
