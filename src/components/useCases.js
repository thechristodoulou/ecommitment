import React  from 'react';
import Header from './header'
import { Container, Row, Col } from 'reactstrap';

class UseCases extends React.Component {

    render() {
        return (
            <Container>

            <Row>
                <Header/>
            </Row>

            <Row className="mt-4">
                <Col>
                    <h5>Happy Use Case</h5>
                    <ol className="mt-2">
                        <li>Some people want to commit to finishing a project.</li>
                        <li>To that end, they each set some money aside.</li>
                        <li>They being working on their project...</li>
                        <li>Some time passes. Eventually, they finish the project.</li>
                        <li>All three agree that the project has been completed to their mutual satisfaction.</li>
                        <li>The money they set aside is fully returned to each person.</li>
                    </ol>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col>
                    <h5>Unhappy Use Case</h5>
                    <ol className="mt-2">
                        <li>Some people want to commit to finishing a project.</li>
                        <li>To that end, they each set some money aside.</li>
                        <li>They being working on their project...</li>
                        <li>Some time passes. One of them refuses to continue work and leaves the project.</li>
                        <li>The project is not completed. Not everyone is satisfied. </li>
                        <li>The money they set aside is returned to them, but a considerable penalty has been deducted from it.</li>
                    </ol>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col>
                    Think <a target="_blank" rel="noopener noreferrer" href="https://en.wikipedia.org/wiki/Escrow">escrows</a>, but without any legal contract (or lawyer) involved.
                </Col>
            </Row>

            </Container>
        )
    }
}

export default UseCases
