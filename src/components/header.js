import React from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { fetchAccountViaWeb3 } from '../actions/account';
import  { Redirect } from 'react-router-dom'

class Header extends React.Component {

    render() {
        let instance = this;
            return (
                 <Container>
                    { this.props.account.error &&
                        <Redirect to="/error"/>
                    }
                    <Row>
                        <Col>
                            <h1 className="mt-2 mb-0">eCommitment</h1>
                        </Col>
                    </Row>
                    <Row >
                        <Col>
                            <h5>Commit to doing things, using Ether</h5>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <p dangerouslySetInnerHTML={{ __html: instance.createWelcomeMessageHtml() }} />
                        </Col>
                        <Col xs="3">
                            <p className="float-right" dangerouslySetInnerHTML={{ __html: instance.createNetworkInformationHtml() }}/>
                        </Col>
                    </Row>

                    <Row>
                    <Col className="px-0">
                        <nav className="navbar navbar-expand-lg navbar-light navbar-custom">
                            <div className="collapse navbar-collapse">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item"><NavLink className="nav-link" activeClassName="text-body" to="/usecases">Use Cases</NavLink></li>
                                    <li className="nav-item"><NavLink className="nav-link" activeClassName="text-body" to="/howtouse">How To Use</NavLink></li>
                                    <li className="nav-item dropdown">
                                        <a className={ "nav-link dropdown-toggle" + ((instance.isSelected()) ? " text-body": "") } href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Commitments</a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <NavLink className="dropdown-item" to="/commitments">Find a commitment</NavLink>
                                            <NavLink className="dropdown-item" to="/newcommitment">Create new commitment</NavLink>
                                        </div>
                                    </li>
                                    <li className="nav-item"><NavLink className="nav-link" activeClassName="text-body" to="/aboutme">About Me</NavLink></li>
                                </ul>
                                <a className="navbar-mimic" href="mailto: bugaddress??@gmail.com">Report a Bug</a>
                            </div>
                        </nav>
                    </Col>
                </Row>
            </Container>
        )
    }

    componentWillMount() {
        if(!this.props.isInitialized()) {
            this.props.fetchAccountViaWeb3();
            //this.props.fetchAccountViaWeb3();
        }
    }

    createWelcomeMessageHtml() {
        let account = this.props.account;
        if (account.isQuerying) {
            return "Welcome...?";
        } else if (account.error) {
            return "Welcome, and apologies.";
        } else if (account.address) {
            let providedName = (account.provider.name);
            let providerString = (providedName) ? " (via " + providedName + ")" : "";
            return "Welcome, <strong>" + account.address + "</strong>" + providerString;
        } else if (account.address == null) {
            return "Welcome. Please use <a target='_blank' rel='noopener noreferrer' href='https://metamask.io/'>MetaMask</a> to fully utilize this smart contract.";
        }
    }

    createNetworkInformationHtml() {
        let account = this.props.account;
        if(this.props.isQuerying()) {
            return "Network: <strong>Querying...</strong>";
        } else if(account.error) {
            return "Network: ?";
        } else {
            return "Network: <strong>" + account.network.name + "</strong>";
        }
    }

    // hack to prettify the header a little bit
    isSelected() {
        return this.props.currentPath.includes('commitment');
    }
}

function mapStateToProps(state) {
    return {
        currentPath: state.router.location.pathname,
        account: state.account,
        isInitialized: function() {
            return state.account.address !== undefined;
        },
        isQuerying: function() {
            return state.account.isQuerying || state.account.isQuerying === undefined;
        }
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchAccountViaWeb3: function() {
            dispatch(fetchAccountViaWeb3());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
