import React  from 'react';
import Header from './header'
import { Container, Row, Col } from 'reactstrap';

class HowToUse extends React.Component {

    render() {
        return (
            <Container>

            <Row>
                <Header/>
            </Row>

            <Row className="mt-4">
                <Col>
                    <h5>With Metamask</h5>
                    <ol className="mt-2">
                        <li>Use the navigation bar to go to the new commitment page.</li>
                        <li>Enter the information needed for your commitment. Some notes:</li>
                        <li>The "description" is not a legal document in any way. You should not use it to fully define your commitment. Make sure that any participants have already communicated, understood and agreed on what it is they are committing to.</li>
                        <li>Make sure to enter your address in the participant list, along with everyone else who is participating.</li>
                        <li>??? Something about commitment?stake?</li>
                        <li>??? Something about satisfaction?</li>
                    </ol>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col>
                    <h5>Without Metamask</h5>
                    <div className="ml-3 mt-2">
                        There are several ways to do this. Unfortunately, they are beyond our scope.<br/>
                        For example, <a href="https://www.myetherwallet.com/">MyEtherWallet</a> provides an easy way to consume smart contract functionality.<br/>
                        In any case, you will probably need:<br/>
                        <ul>
                            <li>This contract's <a target="_blank" rel="noopener noreferrer" href="">ABI</a></li>
                            <li>This contract's address: <strong>0xd2b1e7dbf32533012e4e3a4e781d842861bc2c57</strong></li>
                        </ul>
                    </div>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col>
                    I hope this helps you!
                </Col>
            </Row>

            </Container>
        )
    }
}

export default HowToUse
