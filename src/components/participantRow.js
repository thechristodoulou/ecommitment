import React from 'react';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap';
import { depositToCommitment } from '../actions/commitments';
import { getEtherscanHtmlLinkForAddress } from '../utils/miscUtils';

class ParticipantRow extends React.Component {

    constructor(props) {
        super(props);
        //console.log("Inside ParticipantRow")
        //console.log(props)
        this.deposit = this.deposit.bind(this);
    }

    render() {
        if (this.props.participant) {
            return (
                <Row>
                    <Col style={{'margin-top': '5px'}} className="col-5">
                        <span style={{"float": "left"}}>&#8226;
                        { getEtherscanHtmlLinkForAddress(this.props.networkId, this.props.participant.address) }
                        {/* <br/>{ this.getParticipantDepositStatus() }  */}
                        </span>
                    </Col>
                    <div className="col-7">

                        { this.isCurrentUserParticipant() && this.hasDeposited() && 
                            <div style={{"float": "left"}} className="dropdown">
                            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Set Status
                            </button>
                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a className="dropdown-item" href="#">Satisfied</a>
                                <a className="dropdown-item" href="#">Unsatisfied</a>
                            </div>
                        </div>
                        }
                        { this.isCurrentUserParticipant() && !this.hasDeposited() && 
                            <button style={{"float": "left"}} id="deposit" type="button" className="btn btn-light" onClick={this.deposit}>Deposit</button>
                        }

                        { this.isCurrentUserParticipant() && this.hasDeposited() && this.props.participant.satisfactionState === 'UNSATISFIED' &&
                            <div>Unsatisfied</div>
                        }

                        { this.isCurrentUserParticipant() && this.hasDeposited() && this.props.participant.satisfactionState === 'SATISFIED' &&
                            <div>Satisfied</div>
                        }

                        {/* These are the cases when the user viewing this contract is NOT the one inside this row  */}
                        { !this.isCurrentUserParticipant() && !this.hasDeposited() &&
                            <div>Not yet deposited</div>
                        } 

                        { !this.isCurrentUserParticipant() && this.hasDeposited() && this.props.participant.satisfactionState === 'UNDECIDED' &&
                            <div>Not yet decided</div>
                        }

                        { !this.isCurrentUserParticipant() && this.hasDeposited() && this.props.participant.satisfactionState === 'SATISFIED' &&
                            <div>Satisfied</div>
                        } 

                        { !this.isCurrentUserParticipant() && this.hasDeposited() && this.props.participant.satisfactionState === 'UNSATISFIED' &&
                            <div>Unsatisfied</div>
                        }     

                    </div>
                </Row>
            )
        } else {
            return null;
        }
    }

    getParticipantDepositStatus() {
        let hasDepositedText = (this.props.participant.hasDeposited) ? "Has Deposited." : "Has not Deposited.";
        return hasDepositedText;
    }

    getParticipantSatisfactionStatus() {
        //return (this.props.participant[2]) ? "Satisfied." : "Unsatisfied.";
        switch(this.props.participant.satisfactionState) {
            case "UNDECIDED":
                return "Yet undecided.";
            case "SATISFIED":
                return "Satisfied.";
            case "UNSATISFIED":
                return "Not satisfied.";
        }
    }

    getSatisfactionButton() {
        // nothing yet
    }

    getStatusMessage() {
        return ""
    }

    hasDeposited() {
        return this.props.participant.hasDeposited;
    }

    isSatisfied() {

    }

    deposit() {
        this.props.deposit(this.props.account.provider.web3, this.props.account.network.id, this.props.commitmentId,
            this.props.depositWei);
    }

    /*
    * is this commitment accessed by a user who is a member of it?
    */
    isCurrentUserParticipant() {
        return this.props.participant.address === this.props.account.address;
    }
}

function mapStateToProps(state) {
    return {
        account: state.account,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        deposit: function(web3, networkId, commitmentId, depositWei) {
            dispatch(depositToCommitment(web3, networkId, commitmentId, depositWei));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ParticipantRow);
