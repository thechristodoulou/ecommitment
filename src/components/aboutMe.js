import React  from 'react';
import Header from './header'
import { Container, Row, Col } from 'reactstrap';

class AboutMe extends React.Component {

    render() {
        return (
            <Container>
                <Row>
                    <Header/>
                </Row>
                <Row>
                    <Col>
                        <div className="pt-3 pl-3">
                            <p>My name is Theoklitos Christodoulou, and I am a Software Engineer.</p>
                            <p>I am interested in the Blockchain, Smart Contracts, Cryptocurrencies, Java and also Quality Assurance.</p>
                            <p>If you want to build something, please <a href="mailto: thechristodoulou@hotmail.com">send me a message</a>:)</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default AboutMe
