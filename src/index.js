import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route } from 'react-router'
import { createBrowserHistory } from 'history';
import { connectRouter/*, routerMiddleware*/ } from 'connected-react-router'
import UseCases from './components/useCases'
import HowToUse from './components/howToUse'
import CommitmentSearch from './components/commitmentSearch'
import NewCommitment from './components/newCommitment'
import AboutMe from './components/aboutMe'
import FourOhFour from './components/errors/fourOhFour'
import ErrorPage from './components/errors/errorPage'

import { commitments } from './reducers/commitments'
import { account } from './reducers/account'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap'
import './css/ecommitment.css'

const history = createBrowserHistory();

const combinedReducers = combineReducers({
    commitments,
    account
});

const store = createStore(connectRouter(history)(combinedReducers), {
        commitments: {},
        account: { provider: { web3: undefined }}
    },
    applyMiddleware(
        thunkMiddleware,
        createLogger()
    )
)

ReactDOM.render(
  <Provider store={store}>
    {}
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact={true} path="/" component={UseCases} />
        <Route path="/usecases" component={UseCases} />
        <Route path="/howtouse" component={HowToUse} />
        <Route path="/commitments/:id?" component={CommitmentSearch} />
        <Route path="/newcommitment" component={NewCommitment} />
        <Route path="/aboutme" component={AboutMe} />
        // the error stuff:
        <Route path="/error" component={ErrorPage} />
        <Route component={FourOhFour} />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root') || document.createElement('div')
)
