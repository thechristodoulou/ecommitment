import React from 'react';
import { Provider } from 'react-redux'
import Enzyme, { mount, shallow, unmount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from './components/header';
import Index from './index';
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { BrowserRouter } from 'react-router-dom'
import { account } from './reducers/account'
import renderer from 'react-test-renderer';

Enzyme.configure({ adapter: new Adapter() });

describe('Header Tests', () => {

    let container = undefined;
    let store = undefined;
    let props = {}

    beforeEach(() =>{
      store = createStore(
        account,
        {commitments: {}, account: {provider: { web3: undefined }}},
        applyMiddleware(thunkMiddleware)
      );
      // const container1 = shallow(<BrowserRouter><Header store={store} {...props}/></BrowserRouter>);
    })

    it('renders the app without crashing', () => {
      expect(JSON.stringify(Index)).toMatchSnapshot();
    });

    it('renders the header', () => {
      const container = shallow(<BrowserRouter><Header store={store}/></BrowserRouter>);
      const tree = renderer.create(container).toJSON();
      expect(tree).toMatchSnapshot();
      expect(container.html()).toContain('eCommitment')
      container.unmount();
    })

    it('queries for the network info', () => {
      const container = mount(<BrowserRouter><Header store={store}/></BrowserRouter>);
      expect(container.html()).toContain('Network: <strong>Querying...</strong>')
      container.unmount();
    })

    it('throws an error on network querying', () => {
      const props = {isQuerying: false, error: true}
      const container = shallow(<BrowserRouter><Header store={store}/></BrowserRouter>);
      const header = container.dive().dive().dive();
      header.setProps(props);
      const network = header.instance().createNetworkInfo()
      expect(network).toMatch('Network: ?')
      container.unmount()
    })

    it('shows the network name on successful fetch', () => {
      const props = {isQuerying: false, error: false, network: {name: 'Tina'}}
      const container = shallow(<BrowserRouter><Header store={store}/></BrowserRouter>);
      const header = container.dive().dive().dive();
      header.setProps(props);
      const network = header.instance().createNetworkInfo()
      expect(network).toMatch('Network: <strong>Tina</strong>')
      container.unmount()
    })

});
