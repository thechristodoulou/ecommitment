/*
* Is the value numeric and non-zero?
*/
export function isNumeric(value) {
    return value && !isNaN(value, 0) && isFinite(value);
}

// this expects a log object as returned from web3.js
export function getValueFromFromTxLog(eventName, key, logObject) {
    let filteredEvents = logObject.filter(function(element) {
        if (element.event === eventName) {
            return element;
        }
        return ""; // TODO test
    });

    if(!filteredEvents || filteredEvents.length === 0) {
        return "";
    } else {
        return filteredEvents[0].args[key];
    }
}

export function getEtherscanHtmlLinkForAddress(networkId, address) {
    let text = address
    let prefix;
    let actualAddress;
    switch (networkId) {
        case "1":
            prefix =  "https://etherscan.io/address/";
            break;
        case "3":
            prefix = "https://ropsten.etherscan.io/address/";
            break;
        case "4":
            prefix = "https://rinkeby.etherscan.io/address/";
            break;
        case "42":
            prefix = "https://kovan.etherscan.io/address/";
            break;
        case "5777":
        default:
            prefix = "";
    }
    actualAddress = prefix + address;
    return (prefix) ? "<a target='_blank' rel='noopener noreferrer' href='" + actualAddress + "'>" + text + "</a>" : text;
}
