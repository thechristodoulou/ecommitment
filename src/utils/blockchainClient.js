// use some kind of conditional import. needs babel update
// if (process.env.NODE_ENV === "development") {
//import ECommitment from '../../build/contracts/ECommitment.json'
import ECommitment from '../../abi/ECommitment-latest.json'
import contract from 'truffle-contract'
import addresses from '../addresses'

export function fetchCommitment(web3, networkId, contractId) {
    return getDeployedContractPromise(web3, networkId).then((instance) => {
        return instance.commitments(contractId);
    });
}

export function fetchParticipants(web3, contractId, networkId, noOfParticipants) {
    return getDeployedContractPromise(web3, networkId).then(async (instance) => {
        let participants = [];
        for(let i = 0; i < noOfParticipants; i++) {
            let participant = await instance.participants(contractId, i);
            participants.push(participant);
        }
        return participants;
    });
}

export function createNewCommitment(web3, networkId, participants, depositWei, penaltyPercentage, description) {
    let _description = (description) ? description : "";
    return getDeployedContractPromise(web3, networkId).then(instance => {
        let result = instance.makeCommitment(participants, depositWei, penaltyPercentage, _description, {
            from: web3.eth.accounts[0],
            gas: 150000
        });
		return result;
    });
}

export function depositToCommitment(web3, networkId, commitmentId, depositWei) {
    return getDeployedContractPromise(web3, networkId).then(instance => {
        console.log("Blockchain client depositToCommitment():")
        console.log(depositWei)
        console.log(web3.eth.accounts)
        console.log(web3.eth.accounts[0])
        console.log("=====================================")
        let result = instance.commit(commitmentId, {
            value: depositWei,
            from: web3.eth.accounts[0],
            gas: 200000
        });
        return result;
    });
}

function getDeployedContractPromise(web3, networkId) {
    let eCommitment = contract(ECommitment);
    eCommitment.setProvider(web3.currentProvider);
    let resolvedAddress = addresses[networkId];
    return eCommitment.at(resolvedAddress);
}
