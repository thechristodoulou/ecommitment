import Web3 from 'web3'

let getWeb3Promise = new Promise(function(resolve, reject) {

    function initializeWithMetamask() {
        let web3 = new Web3(window.web3.currentProvider);
        let providerName = web3.currentProvider.constructor.name.includes('Metamask') ? "MetaMask" : ""; // TODO are there others?
        let account = {
            web3: web3,
            providerName: providerName
        }
        console.log('Using Metamask-injected web3.');
        resolve(account);
    }

    // the use case where the user doesn't have anything like Metamask
    // in this case we need to use some kind of "blockchain access provder" for lack of a better term
    // see: https://infura.io/docs/gettingStarted/chooseaNetwork
    function initializeWithThirdPartyProvider() {
        let web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/d2c6874ae2fa4c4cb32016557a7c9502d2c6874ae2fa4c4cb32016557a7c9502"));
        let account = {
            web3: web3,
            providerName: "Infura"
        }
        //resolve(account);
        reject({
            noWeb3js: true
        });
    }

    window.addEventListener('load', function() { // TODO move this to a component?
        if (process.env.NODE_ENV === "development") {
            // this is how you detect a dev build (when using the truffle box)
            // any special handling (e.g. pointing to a local node) should be done here
            console.warn('Development build. THIS SHOULD ONLY BE SEEN DURING DEVELOPMENT!');
        }

        if (typeof window.web3 !== 'undefined') {
            initializeWithMetamask();
        } else {
            initializeWithThirdPartyProvider();
        }
    })
})

export default getWeb3Promise
