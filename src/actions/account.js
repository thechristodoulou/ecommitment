export const REQUEST_ACCOUNT = 'REQUEST_ACCOUNT'
export const RECEIVE_ACCOUNT = 'RECEIVE_ACCOUNT'
export const REQUEST_ACCOUNT_ERROR = 'REQUEST_ACCOUNT_ERROR'

import getWeb3Promise from '../utils/getWeb3'

function requestAccount() {
    return {
        type: REQUEST_ACCOUNT
    }
}

function receiveAccount(address, provider, network) {
    return {
        type: RECEIVE_ACCOUNT,
        address: address,
        provider: provider,
        network: network
    }
}

function requestAccountError(error) {
    return {
        type: REQUEST_ACCOUNT_ERROR,
        error: error
    }
}

function resolveNetworkName(networkId) {
    switch (networkId) {
    case "1":
        return "MainNet";
    case "2":
        return "Morden (Deprecated)";
    case "3":
        return "Ropsten";
    case "4":
        return "Rinkeby";
    case "42":
        return "Kovan";
    case "5777":
        return "Local Ganache";
    default:
        return "Unknown Network";
    }
}

/*
* promisifies one of web3.js functions. this can be done to all web3 methods:
* from: https://ethereum.stackexchange.com/questions/11444/web3-js-with-promisified-api
*/
function promisifyNetworkInformationCall(web3) {
    return new Promise (function (resolve, reject) {
        web3.version.getNetwork(function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        });
    });
}

export function fetchAccountViaWeb3() {
    return function (dispatch) {
        dispatch(requestAccount());
        getWeb3Promise.then(results => {
            //let promisify = promisifyNetworkInformationCall;

            // get the account
            results.web3.eth.getAccounts((error, accounts) => {
                // Use the first account only. I am not sure how I would handle multiple ones (or why there would be multiple accounts)
                let address = (accounts.length === 0) ? null : accounts[0];
                // TODO accounts of length zero means that metamask has no accounts or is locked. if metamask is logged out, this is null. new error type?
                console.log(address)

                if (error) {
                    console.error("1");
                    console.error(error);
                    dispatch(requestAccountError(error));
                    return;
                }

                // then the network
                results.web3.version.getNetwork((error, result) => {
                    if (error) {
                        console.error("2");
                        if (error.toString().includes("request failed")) {
                            // why was this here?
                        }
                        console.error(error);
                        dispatch(requestAccountError(error));
                        return;
                    }
                    let networkId = result;

                    // finally, we have all the info we need. dispatch it.
                    dispatch(receiveAccount(address, {
                        web3: results.web3,
                        name: results.providerName
                    },{
                        id: networkId,
                        name: resolveNetworkName(networkId)
                    }));
                });
            });
        }).catch(error => {
            console.log(error)
            if (error.type === "no-Web3js") {
                console.error("Please use MetaMask.");
            } else {
                console.error(error);
            }
            dispatch(requestAccountError(error));
        })
    }
}
