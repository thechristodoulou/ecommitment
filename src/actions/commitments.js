import * as blockchainClient from '../utils/blockchainClient'
export const REQUEST_COMMITMENT = 'REQUEST_COMMITMENT'
export const RECEIVE_COMMITMENT = 'RECEIVE_COMMITMENT'
export const REQUEST_COMMITMENT_ERROR = 'REQUEST_COMMITMENT_ERROR'
export const SUBMIT_COMMITMENT_ERROR = 'SUBMIT_COMMITMENT_ERROR'
export const SUBMIT_NEW_COMMITMENT = 'SUBMIT_NEW_COMMITMENT'
export const CREATED_COMMITMENT = 'CREATED_COMMITMENT'
export const REMOVE_COMMITMENT = 'REMOVE_COMMITMENT'
export const DEPOSIT_REQUEST = 'DEPOSIT_REQUEST'
export const DEPOSIT_REQUEST_ERROR = 'DEPOSIT_REQUEST_ERROR'
export const DEPOSIT_SUCCESSFUL = 'DEPOSIT_SUCCESSFUL'


function requestCommitment() {
    return {
        type: REQUEST_COMMITMENT
    }
}

function receiveCommitment(commitment) {
    return {
        type: RECEIVE_COMMITMENT,
        commitment: commitment
    }
}

function requestCommitmentError(message) {
    return {
        type: REQUEST_COMMITMENT_ERROR,
        message: message
    }
}

function submitCommitmentError(message) {
    return {
        type: SUBMIT_COMMITMENT_ERROR,
        message: message
    }
}

function submitNewCommitment() {
    return {
        type: SUBMIT_NEW_COMMITMENT
    }
}

function createdCommitment(response) {
    return {
        type: CREATED_COMMITMENT,
        tx: response.tx,
        logs: response.logs,
        receipt: response.receipt
    }
}

export function removeCommitment() {
    return {
        type: REMOVE_COMMITMENT
    }
}

export function depositRequest() {
    return {
        type: DEPOSIT_REQUEST
    }
}

export function depositSuccessful() {
    return {
        type: DEPOSIT_SUCCESSFUL
    }
}

export function fetchCommitmentAndParticipants(web3, networkId, commitmentId) {
    return function (dispatch) {
        dispatch(requestCommitment());
        let commitmentObject = {};
        blockchainClient.fetchCommitment(web3, networkId, commitmentId).then(response => {
            commitmentObject = convertBlockchainResponseToContractObject(response);
            // must check after created if it is a real commitment because solidity doesn't have nulls
            if (isCommitment(commitmentObject)) {
                return blockchainClient.fetchParticipants(web3, commitmentId, networkId, commitmentObject.noOfParticpants);
            } else {
                throw Error("commitment not found");
            }
        }).then(response => {
            if(response !== undefined) {
                // convert the raw resposne object to something prettier
                commitmentObject.participants = response.map(participant => { 
                    return convertBlockchainResponseToParticipantObject(participant)
                });
                dispatch(receiveCommitment(commitmentObject));
            }
        }).catch(error => {
            handleCommitmentError(dispatch, error);
        });
    }
}

export function depositToCommitment(web3, networkId, commitmentId, depositWei) {
    return function (dispatch) {
        dispatch(depositRequest());
        blockchainClient.depositToCommitment(web3, networkId, commitmentId, depositWei).then(response => {
            console.log(response);
            dispatch(depositSuccessful());
        }).catch(error => {
            console.error(error);
            //handleCommitmentError(dispatch, error);
        });
    }
}

export function createNewCommitment(web3, networkId, participantsArray, depositEther, penaltyPercentage, description) {
    return function (dispatch) {
        dispatch(submitNewCommitment());
        blockchainClient.createNewCommitment(web3, networkId, participantsArray, depositEther, penaltyPercentage, description).then(response => {
            dispatch(createdCommitment(response));
        }).catch(error => {
            handleCommitmentError(dispatch, error);
        });
    }
}

// a crude way of differentiating between received commitments errors
// its hard to differentiate between web3.js error messages. Read this: https://ethereum.stackexchange.com/questions/45836/elegant-way-to-map-web3-js-error-messages
function handleCommitmentError(dispatch, error) {
    let errorMessage = (error.message !== undefined) ? error.message : error;
    if (!errorMessage.includes("commitment not found")) { // dont log this case, not a real error
        console.error(error);
    }
    // error while searching for commitment
    if(errorMessage.includes("no code at address")) {
        dispatch(requestCommitmentError("Contract not found, check your address(es)."));
    } else if(errorMessage.includes("commitment not found")) {
        dispatch(requestCommitmentError("Not found."));
    } else if (errorMessage.includes('User denied transaction') || errorMessage.includes('Request has been rejected.') ||
        errorMessage.includes('transaction has been discarded') || errorMessage.includes('Transaction not confirmed')) {
        dispatch(submitCommitmentError("User rejected transation."));
    } else if(errorMessage.includes("nonce too low")) { // =============== have not tested these error cases yet =========
        dispatch(submitCommitmentError("Nonce too low."));
    } else if(errorMessage.includes("nonce may not be larger than")) {
        dispatch(submitCommitmentError("Nonce too high."));
    } else if(errorMessage.includes("insufficient funds for gas")) {
        dispatch(submitCommitmentError("Insufficient funds for gas."));
    } else if(errorMessage.includes("intrinsic gas too low")) { // https://ethereum.stackexchange.com/questions/1570/mist-what-does-intrinsic-gas-too-low-mean
        dispatch(submitCommitmentError("Transaction ran out of gas."));
    } else if(errorMessage.includes('out of gas')) {
        // this really shouldn't happen. its our fault if it does.
        dispatch(submitCommitmentError("Not enough gas for transaction."));
    } else if(errorMessage.includes("invalid address")) { // this is a major issue. redirect?
        // this is probably due to wrong smart contract address or smart contract not deployed
        // it could be either: the ABI is wrong OR the submitted eth addresses are in mixed-case
        console.log(error)
        dispatch(submitCommitmentError("Something is wrong with your account address."));
    } else {
        console.warn("Unexpected error. This should be investigated.");
        dispatch(submitCommitmentError("Unexpected error, transaction not submitted.")); // TODO better message
    }

    // TODO more errorneous cases
    // connectivity stuff?

    // contract does not exist at adddress
    // found message! looks like "cannot create instance of contract no code at" SEEE ABOVE TODO

    // unknown error
}

/*
* TODO explain
*/
function isCommitment(commitmentObject) {
    return !(commitmentObject.noOfParticpants === 0 && commitmentObject.depositWei === 0 &&
        commitmentObject.penaltyPercentage === 0 && commitmentObject.description === "" && commitmentObject.state === 0);
}
// beautification
function convertBlockchainResponseToParticipantObject(input) {
    let niceObject = {};
    niceObject.address = String(input[0], 0);
    niceObject.hasDeposited = input[1];
    switch(parseInt(input[2], 0)) {
        case 0:
            niceObject.satisfactionState = "UNDECIDED"
            break;
        case 1:
            niceObject.satisfactionState = "SATISFIED"
            break;
        case 2:
            niceObject.satisfactionState = "UNSATISFIED"
            break;
    }
    return niceObject;
}

// beautification
function convertBlockchainResponseToContractObject(input) {
    let niceObject = {};
    niceObject.id = parseInt(input[0], 0);
    niceObject.noOfParticpants = parseInt(input[1], 0);
    niceObject.depositWei = parseInt(input[2], 0);
    niceObject.penaltyPercentage = parseInt(input[3], 0);
    niceObject.description = String(input[4], 0);
    niceObject.state = parseInt(input[5], 0);
    return niceObject;
}
