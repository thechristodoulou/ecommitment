import {
    REQUEST_COMMITMENT, RECEIVE_COMMITMENT, REQUEST_COMMITMENT_ERROR, SUBMIT_COMMITMENT_ERROR, SUBMIT_NEW_COMMITMENT,
    CREATED_COMMITMENT, REMOVE_COMMITMENT, DEPOSIT_REQUEST, DEPOSIT_SUCCESSFUL, DEPOSIT_REQUEST_ERROR} from "../actions/commitments.js";
import { getValueFromFromTxLog } from '../utils/miscUtils';

const initialState = {
    isQuerying: false,
    error: undefined,
    buffer: undefined // holds 1 commitment temporarily. Used when searching or creating new commitments.
}

export function commitments(state = initialState, action) {
    switch (action.type) {
        case REQUEST_COMMITMENT:
            return Object.assign({}, state, {
                isQuerying: true,
                buffer: undefined
            })
        case RECEIVE_COMMITMENT:
            return Object.assign({}, state, {
                isQuerying: false,
                error: undefined,
                buffer: action.commitment
            })
        case REQUEST_COMMITMENT_ERROR:
            return Object.assign({}, state, {
                isQuerying: false,
                error: {
                    message: action.message
                },
                buffer: undefined
            })
        case SUBMIT_NEW_COMMITMENT:
            return Object.assign({}, state, {
                isQuerying: true,
                error: undefined,
                buffer: undefined
            })
        case SUBMIT_COMMITMENT_ERROR: // when the transaction was not accepted, for whatever reason
            return Object.assign({}, state, {
                isQuerying: false,
                error: {
                    message: action.message
                },
                buffer: undefined
            })
        case CREATED_COMMITMENT:
            return Object.assign({}, state, {
                isQuerying: false,
                buffer: {
                    id: getValueFromFromTxLog("NewCommitment", "id", action.logs).c[0],
                    tx: action.tx
                },
                error: undefined
            })
        case REMOVE_COMMITMENT:
            return Object.assign({}, state, {
                error: undefined,
                buffer: undefined
            })
        case DEPOSIT_REQUEST:
            return Object.assign({}, state, {
                isQuerying: true,
                error: {
                    message: action.message
                }
            })
        case DEPOSIT_SUCCESSFUL:
            return Object.assign({}, state, {
                isQuerying: false,
                error: undefined,
                buffer: {
                    tx: action.tx
                }
            })
        case DEPOSIT_REQUEST_ERROR:
            return Object.assign({}, state, {
                isQuerying: true,
                error: action,
        })
        default:
            return state;
    }
}
