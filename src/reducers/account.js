import {
    REQUEST_ACCOUNT, RECEIVE_ACCOUNT, REQUEST_ACCOUNT_ERROR
} from "../actions/account";

const initialState = {
    isQuerying: false,
    error: undefined,
    provider: {
        web3: undefined,
        name: undefined
    },
    network: {
        id: undefined,
        name: undefined
    },
    address: undefined
}

export function account(state = initialState, action) {
    switch (action.type) {
        case REQUEST_ACCOUNT:
            return Object.assign({}, state, {
                isQuerying: true,
                error: undefined,
            })
        case RECEIVE_ACCOUNT:
            return Object.assign({}, state, {
                isQuerying: false,
                error: undefined,
                address: action.address,
                provider: Object.assign({}, state.provider, action.provider),
                network: action.network
            })
        case REQUEST_ACCOUNT_ERROR:
            console.log(action.error);
            return Object.assign({}, state, {
                isQuerying: false,
                error: action.error,
                provider: { web3: undefined, name: undefined },
                network: { id: undefined, name: undefined },
                address: undefined
            })
        case RECEIVE_ACCOUNT:
            return Object.assign({}, state, {
                isQuerying: false,
                error: action.error,
                provider: { web3: undefined, name: undefined },
                network: { id: undefined, name: undefined },
                address: undefined
            })
        default:
            return state;
    }
}
